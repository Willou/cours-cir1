<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <form method="POST" action="./control.php">
        <input type="submit" value="disconnect" name="disconnect"/>
  </form>
    <?php
        createCalendar();
    ?>
    <form method="POST" action="./control.php">
  		<input type="submit" value="Previous" name="previous">
  		 - <?php $months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
               $date = checkMonth();
     	         echo $months[$date-1] . " " . checkYear();
        ?> -
  		<input type="submit" value="Next" name="next">
    </form>
  </body>
</html>
