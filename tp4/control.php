<?php
$ini_array = parse_ini_file("secrets.ini",true);
session_start();
if(isset($_POST['submit'])){
  if(isset($_POST['login'])){
    $login = $_POST['login'];
  }
  else{
    $login = null;
  }
  if(isset($_POST['passwd'])){
    $password = $_POST['passwd'];
  }
  else{
    $password=null;
  }
  try{
    $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
    $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
    $query = "SELECT * FROM Users WHERE login = :log";
    $statements = $bdd->prepare($query);
    $statements->execute([":log" => $login]);
    foreach ($statements as $row) {
      if(password_verify($password, $row["password"])){
        $_SESSION["rank"] = $row["rank"];
        $_SESSION["id"] = $row["id"];
        $_SESSION["name"] = $row["login"];
      }
      else{
        header("Location: index.php");
        echo("Incorrect password and/or login. Please try again.");
      }
    }
  }
  catch(Exception $e){
    exit("Failed connection");
  }
  header("Location: index.php");
}
if(filter_input(INPUT_POST, "submit", FILTER_SANITIZE_SPECIAL_CHARS) && !filter_input(INPUT_POST, "rank", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["connectionError"] = "Error while connecting";

}

if($_SESSION["rank"] == "CUSTOMER"){
  include("print.php");
}

if(filter_input(INPUT_POST, "disconnect", FILTER_SANITIZE_SPECIAL_CHARS)){
  session_destroy();
  header("Location: index.php");
}

function createCalendar(){
  echo "<table class=\"calendar\">";
  echo "<tr><td>Lundi</td><td>Mardi</td><td>Mercredi</td><td>Jeudi</td><td>Vendredi</td><td>Samedi</td><td>Dimanche</td></tr>";
  $nbrDays = cal_days_in_month(CAL_GREGORIAN, checkMonth(), checkYear());
  $dowMap = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  $firstDay = date("N", strtotime("first day of " . $dowMap[checkMonth()-1] . " " . checkYear()));
  $j = 1;
  while($j < $firstDay){

    if($j % 7 == 1) echo "<tr>";
    echo "<td>-</td>";
    if($j % 7 == 0) echo "</tr>";

    $j++;
  }
  for($i = 1; $i <= $nbrDays; $i++){

if(($i+($firstDay-1)) % 7 == 1) echo "<tr>";
  echo "<td class=\"case\"><div class=\"day\">" . $i . "</div><div class=\"informations\">";
}
  echo "</table>";
}

function checkMonth(){
  if(isset($_SESSION['month']))
    $month = htmlspecialchars($_SESSION['month']);
  else
    $month = $_SESSION['month'] = date("m");
  return $month;
}

function checkYear(){
  if(isset($_SESSION['year']))
    $year = $_SESSION['year'];
  else
    $year = $_SESSION['year'] = date('Y');
  return $year;
}

function nextMonth(){
  if($_SESSION['month'] == 12){
    $_SESSION['month'] = 1;
    $_SESSION['year'] = $_SESSION['year'] + 1;
  }
  else
    $_SESSION['month'] = $_SESSION['month'] + 1;
}

function prevMonth(){
  if($_SESSION['month'] == 1){
    $_SESSION['month'] = 12;
    $_SESSION['year'] = $_SESSION['year'] - 1;
  }
  else
    $_SESSION['month'] = $_SESSION['month'] - 1;
}

if(filter_input(INPUT_POST, "next", FILTER_SANITIZE_SPECIAL_CHARS)){
  nextMonth();
  header("Location: index.php");
}

if(filter_input(INPUT_POST, "previous", FILTER_SANITIZE_SPECIAL_CHARS)){
  prevMonth();
  header("Location: index.php");
}
