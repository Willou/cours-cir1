<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="style.css"/>
    <title>
      Calendar
    </title>
  </head>
  <body>
    <?php
      if(isset($_SESSION["rank"], $_SESSION["name"])){
        if($_SESSION["rank"] == "CUSTOMER"){
              include("./index.php");
        }
      }
      else{
        include("./form.html");
      }
    ?>
  </body>
</html>
